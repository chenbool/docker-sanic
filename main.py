from sanic import Sanic
from sanic.response import json

app = Sanic("App")

@app.route("/")
async def index(request):
    for row in range(100000):
        pass
    return json({"msg": "ok"})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=False)